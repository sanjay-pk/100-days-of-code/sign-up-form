
const divEmail = document.getElementById("divEmail");
const divPassword = document.getElementById("divPassword");
const divName = document.getElementById("divName");
const nameInput = document.getElementById("nameIn");
const emailInput = document.getElementById("emailIn");
const passwordInput = document.getElementById("passwordIn");
const submitBtn = document.getElementById("form");
const btnIn = document.getElementById("btnIn")


// Create error elements outside the event listener

const nameError = document.createElement("p");
const emailError = document.createElement("p");
const passwordError = document.createElement("p")


// add eventlistner in form

submitBtn.addEventListener('submit', (event) => {
    event.preventDefault();
    

    const name = nameInput.value;
    const email = emailInput.value;
    const password = passwordInput.value;
    const emailRegex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;


    // name validation

    if (!name) {
        nameError.classList.add("errorP");
        nameError.innerHTML = "Name cannot be blank.";
        divName.appendChild(nameError);
    } else {
        // Clear error message if name is not empty
        nameError.classList.remove("errorP")
        nameError.innerHTML = "";
    }

        //  password validation

    if (!password) {
        passwordError.classList.add("errorP")
        passwordError.innerHTML = "Password cannot be blank."
        divPassword.appendChild(passwordError)
    } 
    else if (!password.match(passwordRegex)) {
        passwordError.classList.add("errorP")
        passwordError.innerHTML = "Password must contain at least one uppercase letter, one lowercase letter, one special character, one digit, and be at least 8 characters long."
        divPassword.appendChild(passwordError)
    }
    
    else {
        passwordError.classList.remove("errorP")
        passwordError.innerHTML = "";
    }

//  email validation

if (!email) {
    emailError.classList.add("errorP")
    emailError.innerHTML = " Email address cannot be blank."
    divEmail.appendChild(emailError)
    console.log(emailError)
}
else if (!email.match(emailRegex)) {
    emailError.classList.add("errorP")
    emailError.innerHTML = "Please enter a valid email address."
    divEmail.appendChild(emailError)
    console.log(emailError)
}
else {
    emailError.classList.remove("errorP")
    emailError.innerHTML = "";
}

if (passwordError.innerHTML == 0 && nameError.innerHTML == 0 && emailError.innerHTML == 0) {
    form.submit();
    window.location.href = "welcome.html";
}
});







